﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Karakter_Top : MonoBehaviour
{
    public float hiz;
    public bool ziplamabool, hizbool, timestarbool, finishbool;
    public int ziplamaint, karaktercan, zamani, zaman, zamanController, hizarttirma, sesint;
    public GameObject panelwon, panellost, timestar, sesler, butondevamet;
    public Sprite finishred;
    public Text cantxt, zamantxt;
    public AudioSource theme, failses, wonses;


    void Start()
    {
        sesint = PlayerPrefs.GetInt("ses");
        if (sesint == 1)
        {
            sesler.gameObject.SetActive(false);
        }
        karaktercan = 100;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        zamani += 1;
        zaman = zamani / 60;
        zamantxt.text = zaman.ToString();
        if (zaman > zamanController)
        {
            timestar.gameObject.SetActive(false);
            timestarbool = true;
        }
        #region cantxt renkleri
        if (karaktercan <= 70 && karaktercan > 40)
            cantxt.color = Color.blue;
        else if (karaktercan <= 40 && karaktercan > 20)
            cantxt.color = Color.yellow;
        else if (karaktercan <= 20)
            cantxt.color = Color.red;


        #endregion
        if (karaktercan <= 0)
        {
            karaktercan = 0;
            theme.Stop();
            zamani -= 1;
            panellost.SetActive(true);
            transform.Translate(-hiz, 0, 0);
            Time.timeScale = 0;
        }

        cantxt.text = karaktercan.ToString();
        if (Time.timeScale == 1)
        {
            transform.Translate(hiz, 0, 0);
        }
        if (ziplamabool)
        {
            ziplamaint--;
            transform.Translate(0, 0.5f, 0);
            if (ziplamaint <= 0)
            {
                ziplamabool = false;
            }
        }
        if (hizbool)
        {
            hizarttirma--;
            if (hizarttirma <= 0)
            {
                hiz = hiz / 3;
                hizbool = false;
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ucma")
        {
            ziplamaint = 50;
            ziplamabool = true;
        }
      /*  if (collision.gameObject.tag == "hiz")
        {
            hizarttirma = 75;
            hizbool = true;
            hiz = hiz * 3;
        }
        */
        if (collision.gameObject.tag == "mermi")
        {
            if (karaktercan == 20)
            {
                failses.Play();
            }
            karaktercan -= 20;

        }
        if (collision.gameObject.tag == "diken")
        {
            failses.Play();
            karaktercan = 0;
            butondevamet.gameObject.SetActive(false);
        }
        if (collision.gameObject.name == "finish")
            collision.gameObject.GetComponent<SpriteRenderer>().sprite = finishred;

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "finish")
        {
            theme.Stop();
            wonses.Play();
            finishbool = true;
            panelwon.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
