﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class levelbutton : MonoBehaviour
{
    public int yildiz, level, bukac,paraodendi;
    public string levelcount;
    public GameObject star1, star2, star3;
    public Text this_leveltxt;
    public bool ucretli_mi;

    void Start()
    {
        paraodendi = PlayerPrefs.GetInt("UnlockLevels");
        level = PlayerPrefs.GetInt("level");
        levelcount = "level" + bukac.ToString();

        this_leveltxt.text = this.name;
        yildiz = PlayerPrefs.GetInt(levelcount);
        if (yildiz == 1)
        {
            star1.gameObject.SetActive(true);
        }
        else if (yildiz == 2)
        {
            star1.gameObject.SetActive(true);
            star2.gameObject.SetActive(true);
        }
        else if (yildiz == 3)
        {
            star1.gameObject.SetActive(true);
            star2.gameObject.SetActive(true);
            star3.gameObject.SetActive(true);
        }
        else
        {
            star1.gameObject.SetActive(false);
            star2.gameObject.SetActive(false);
            star3.gameObject.SetActive(false);
        }

        if (bukac <= level)
        {
            if (ucretli_mi == false)
            {
                this.GetComponent<Button>().interactable = true;
            }
            else if(ucretli_mi == true)
            {
                if (paraodendi == 3)
                {
                    this.GetComponent<Button>().interactable = true;
                }

            }
        }
    }

    void Update()
    {
        
    }
}
