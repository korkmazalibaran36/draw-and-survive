﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPScript : MonoBehaviour
{

    private Sdkbox.IAP _iap;

    // Use this for initialization
    void Start()
    {
        _iap = FindObjectOfType<Sdkbox.IAP>();
        if (_iap == null)
        {
            Debug.Log("Failed to find IAP instance");
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Purchase(string item)
    {
        if (_iap != null)
        {
            Debug.Log("About to purchase " + item);
            _iap.purchase(item);
        }
    }

    /*
     * Event Handlers
     */
    public void onInitialized(bool status)
    {
        Debug.Log("IAPScript.onInitialized " + status);
    }

    public void onSuccess(Sdkbox.Product product)
    {
        Debug.Log("IAPScript.onSuccess: " + product.name);
    }

    public void onFailure(Sdkbox.Product product, string message)
    {
        Debug.Log("IAPScript.onFailure " + message);
    }

    public void onCanceled(Sdkbox.Product product)
    {
        Debug.Log("IAPScript.onCanceled product: " + product.name);
    }

    public void onRestored(Sdkbox.Product product)
    {
        Debug.Log("IAPScript.onRestored: " + product.name);
    }

    public void onProductRequestSuccess(Sdkbox.Product[] products)
    {
        foreach (var p in products)
        {
            Debug.Log("Product: " + p.name + " price: " + p.price);
        }
    }
    public void onProductRequestFailure(string message)
    {
        Debug.Log("IAPScript.onProductRequestFailure: " + message);
    }
    public void onRestoreComplete(string message)
    {
        Debug.Log("IAPScript.onRestoreComplete: " + message);
    }

}

