﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class cizgiolustur : MonoBehaviour
{
    public LineRenderer linerender;
    public EdgeCollider2D edgecol;

    List<Vector2> sayi;

    public void UpdateLine(Vector2 mousepoz)
    {
        if(sayi == null)
        {
            sayi = new List<Vector2>();
            SayiKaydet(mousepoz);
            return;
        }
        if(Vector2.Distance(sayi.Last(), mousepoz) > .1f)
        {
            SayiKaydet(mousepoz);
        }
    }
    public void SayiKaydet(Vector2 sayis)
    {
        sayi.Add(sayis);
        linerender.positionCount = sayi.Count;
        linerender.SetPosition(sayi.Count - 1, sayis);

        if(sayi.Count> 1)
        {
            edgecol.points = sayi.ToArray();
        }
    }
}
