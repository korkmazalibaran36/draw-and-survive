﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mermi : MonoBehaviour
{
    public Animator mermiAnim;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mermiAnim = this.GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Line")
        {
            mermiAnim.Play("Idle");
        }
    }
}
