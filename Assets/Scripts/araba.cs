﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class araba : MonoBehaviour
{
    public GameObject patlak1, patlak2;
    public AudioSource ses_teker, ses_kaza;
    public bool oldu_mu;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(this.GetComponent<Karakter>().karaktercan == 0)
        {
            if (oldu_mu == false)
            {
                ses_kaza.Play();
                oldu_mu = true;
            }

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "pat")
        {
            this.GetComponent<Karakter>().karaktercan = 0;
            this.GetComponent<Karakter>().panellost.SetActive(true);
            this.GetComponent<Animator>().enabled = false;
            patlak1.SetActive(true);
            patlak2.SetActive(true);
            ses_teker.Play();

        }
    }
}
