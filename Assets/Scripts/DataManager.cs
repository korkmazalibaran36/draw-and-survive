﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour
{

    public GameObject UnlockLevelsButton;
    public bool NoAds;
    public int i;
    public Text unlocktext;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        i = PlayerPrefs.GetInt("UnlockLevels");
        unlocktext.text = i.ToString();
      if(i == 20)
        {
            NoAds = true;
            UnlockLevelsButton.gameObject.SetActive(false);
            Debug.LogWarning(i);
        }  
    }
    public void RemoveAds()
    {
        NoAds = true;
        UnlockLevelsButton.gameObject.SetActive(false);
        PlayerPrefs.SetInt("UnlockLevels", 1);
    }
    public void RemoveAdsFail()
    {
        NoAds = false;
        UnlockLevelsButton.gameObject.SetActive(true);
        PlayerPrefs.SetInt("UnlockLevels", 0);

    }
}
