﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mainMenu : MonoBehaviour
{
    public int level,ses, oyuncuno1, oyuncuno2, levelsunlock;
    public Text leveltxt, oyuncu_no, oyuncu_no1;
    public GameObject levelimg, sesacik, seskapali, panelbuy, butonlevelun;
    public AudioSource mainmenuTheme;


    void Start()
    {
        levelsunlock = PlayerPrefs.GetInt("UnlockLevels");
        oyuncuno1 = PlayerPrefs.GetInt("Oyuncu1");
        oyuncuno2 = PlayerPrefs.GetInt("Oyuncu2");
        if(oyuncuno1 == 0)
        {
            oyuncuno1 = Random.Range(0, 10000);
            oyuncuno2 = Random.Range(500, 10000);
            PlayerPrefs.SetInt("Oyuncu1", oyuncuno1);
            PlayerPrefs.SetInt("Oyuncu2", oyuncuno2);
        }
        level = PlayerPrefs.GetInt("level");
        if (level == 0)
        {
            level += 1;
            PlayerPrefs.SetInt("level", level);
        }
    }

    // Update is called once per frame
    void Update()
    {
        levelsunlock = PlayerPrefs.GetInt("UnlockLevels");
        oyuncu_no.text = oyuncuno1.ToString();
        oyuncu_no1.text = oyuncuno2.ToString();
 
        if(levelsunlock == 3)
        {
            butonlevelun.SetActive(false);
        }
        ses = PlayerPrefs.GetInt("ses");
        leveltxt.text = level.ToString();
        if (ses == 0)
        {
            sesacik.SetActive(true);
            seskapali.SetActive(false);
            mainmenuTheme.volume = 1;
        }
        else if (ses == 1)
        {
            sesacik.SetActive(false);
            seskapali.SetActive(true);
            mainmenuTheme.volume = 0;
        }
    }
    public void direktoyna()
    {
        Time.timeScale = 1;
        if (level >= 21)
        {
            if (levelsunlock == 3)
            {
                Application.LoadLevel(level);
            }
            else
            {
                panelbuy.gameObject.SetActive(true);
            }
        }
        else
        {
            Application.LoadLevel(level);

        }
    }
    public void levelsec(int levelcode)
    {
        Time.timeScale = 1;
        if (levelcode < 21)
        {
            Application.LoadLevel(levelcode);
        }
       else if(levelcode >=21)
        {
            if(levelsunlock >= 3)
            {
                Application.LoadLevel(levelcode);

            }
            else
            {
                panelbuy.gameObject.SetActive(true);
            }
        }
        
    }
    public void levelimgackapa(bool acikmi)
    {
        levelimg.SetActive(acikmi);
    }
    public void sesackapa()
    {
        if(ses == 0)
        {
            PlayerPrefs.SetInt("ses", 1);
        }
        if (ses == 1)
        {
            PlayerPrefs.SetInt("ses", 0);
        }
    }
    public void panelbuykapa()
    {
        panelbuy.gameObject.SetActive(false);

    }
    public void panelbuyac()
    {
        panelbuy.gameObject.SetActive(true);
    }
}
