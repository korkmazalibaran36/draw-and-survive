﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class yildizlar : MonoBehaviour
{
    public int level, yildiz, levelkontrol, ucretli_mi;
    public bool time, line, finishdegdi, topmu;
    public string levelcount;
    public GameObject panelpause, pausebutton, bannerci;

    void Start()
    {
        if (topmu == false)
        {
            bannerci = GameObject.Find("BannerReklam");
            bannerci.AddComponent<GameDistribution>();
            bannerci.AddComponent<GameManager>();
            bannerci.GetComponent<GameDistribution>().GAME_KEY = "f182746c4e3e477dabe428529784e83a";
        }
       levelkontrol =  PlayerPrefs.GetInt(levelcount);
        level = PlayerPrefs.GetInt("level");
        if (level == 0)
        {
            level = 1;
            PlayerPrefs.SetInt("level", level);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            panelacpause();
        }
        ucretli_mi = PlayerPrefs.GetInt("UnlockLevels");

        if (topmu == false)
        {
            time = GameObject.Find("Karakter").GetComponent<Karakter>().timestarbool;
            line = GameObject.Find("Olusturma").GetComponent<kod1>().linestarbool;
            finishdegdi = GameObject.Find("Karakter").GetComponent<Karakter>().finishbool;
        }
        if (topmu == true)
        {

            time = GameObject.Find("Karakter").GetComponent<Karakter_Top>().timestarbool;
            line = GameObject.Find("Olusturma").GetComponent<kod1>().linestarbool;
            finishdegdi = GameObject.Find("Karakter").GetComponent<Karakter_Top>().finishbool;
        }
    }
    public void yildizvepass()
    {
        yildiz = 3;
        if (topmu == false)
        {
            GameObject.Find("BannerReklam").GetComponent<AdmobBanner>().BannerRemove();
        }
        if (levelkontrol == 0)
        {
            level += 1;
            PlayerPrefs.SetInt("level", level);
        }
        
        if(time == true)
        {
            yildiz -= 1;
        }
        if(line== true)
        {
            yildiz -= 1;
        }
        PlayerPrefs.SetInt(levelcount, yildiz);
        Time.timeScale = 1;
        if (level >= 21)
        {
            if (ucretli_mi == 3)
            {
                Application.LoadLevel(level);
            }
            else if (ucretli_mi < 3)
            {
                Application.LoadLevel("MainMenu");
            } 

        }
        else
        {
            Application.LoadLevel(level);

        }


    }
    public void maindon()
    {

        GameObject.Find("reklamgo").GetComponent<PlayUnityAd>().showAd();
        if (finishdegdi)
        {
            yildiz = 3;
            if (levelkontrol == 0)
            {
                level += 1;
                PlayerPrefs.SetInt("level", level);
            }

            if (time == true)
            {
                yildiz -= 1;
            }
            if (line == true)
            {
                yildiz -= 1;
            }

            Time.timeScale = 1;

            PlayerPrefs.SetInt(levelcount, yildiz);
        }
        if (topmu = false)
        {
            GameObject.Find("BannerReklam").GetComponent<AdmobBanner>().BannerRemove();
        }

        Application.LoadLevel("MainMenu");

    }
    public void reloadscene()
    {

        if (topmu = false)
        {
            GameObject.Find("BannerReklam").GetComponent<AdmobBanner>().BannerRemove();
        }
        Time.timeScale = 1;
        Application.LoadLevel(levelcount);

    }
    public void panelacpause()
    {
        pausebutton.gameObject.SetActive(false);
        panelpause.SetActive(true);
        Time.timeScale = 0;
    }
    public void panelkapapause()
    {
        pausebutton.gameObject.SetActive(true);
        panelpause.SetActive(false);
        Time.timeScale = 1;
    }
}
