﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class kod1 : MonoBehaviour
{
    public GameObject CizgiPrefab, linestar;
    public int linehakki,yildizicin;
    public Text linetxt,linetxtStart;
    public bool linestarbool;

    cizgiolustur cizgiline;

    void Start()
    {
        linetxtStart.text = linehakki.ToString();

    }
    void Update()
    {
        linetxt.text = linehakki.ToString();

        if (linehakki < yildizicin)
        {
            linestar.gameObject.SetActive(false);
            linestarbool = true;
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (linehakki > 0)
            {
                GameObject cizgit = Instantiate(CizgiPrefab);
                cizgiline = cizgit.GetComponent<cizgiolustur>();
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (linehakki > 0)
            {
                cizgiline = null;
                linehakki -= 1;
            }
        }
        if(cizgiline != null)
        {
            Vector2 mousepos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            cizgiline.UpdateLine(mousepos);
        }
    }

}
